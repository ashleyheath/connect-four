from sys import exit
from GameLoop import game_loop
from GameBoard import GameBoard
from Text import *

class TUI:
    """Text based User Interface class."""    
    def __init__(self, saveManager):
        self.saveManager = saveManager
        
    def enter_to_continue(self):
        """Asks the user to press enter to continue."""
        print '\nPress enter to continue',
        tmp = raw_input('...\n')
        
    def handle_results(self, result, winner):
        """Outputs the relevent result information based on information provided by a game loop."""
        if result == 'WON':
            print '\n'+winner+' wins!'+'\n'
        elif result == 'FULL':
            print '''\nIt's a draw.\n'''
        self.enter_to_continue()

    def game_loop_choice(self, board):
        """A loop to handle the player's choice of where to drop their counter."""
        actualBoardWidth = board.return_board_width()
        actualBoardHeight = board.return_board_height()
        while(True):
            choice = raw_input('Pick a column to drop your counter in (enter m for the in-game menu)...')
            # Check that the string isn't empty i.e. ''
            if choice == '' or None:
                    print_error('Empty Input: You provided no input.')
                    continue
            elif choice == 'm' or choice == 'M':
                print_game_paused()
                self.in_game_menu_loop(board)
                # Re-draw the board
                print_board(board)
                continue
            # Check choice is a number
            malformedInput = False
            for i in choice:
                if i not in '0123456789':
                    print_error('Malformed Input: ' + str(i) + ' is not a valid input.')
                    malformedInput = True
                    break
            if malformedInput:
                continue
            # Check choice is a number within the number of columns on the board
            if int(choice) > actualBoardWidth or int(choice) < 1:
                print_error('Invalid Input: ' + str(choice) + ' is not a valid input.')
            return int(choice)

    def new_game_menu_loop(self):
        """A loop that displays the new game menu."""
        def get_board_size():
            """A loop to request the size of a new board from the player."""
            while(True):
                size = raw_input('Please enter a number to be the size of your board (hit enter to use the standard board size)...')
                if size == '\n' or size == '' or size == ' ':
                    size = '10'
                numbers = '0123456789'
                error = False
                for i in size:
                    if i not in numbers:
                        print_error('%s is not a valid option' %size)
                        error = True
                        break
                if error:
                    continue
                if int(size) < 5:
                    print_error('Board size too small')
                    continue
                if int(size) > 15:
                    print_error('Board size too large')
                    continue
                return int(size)
            
        player1counter = 'O'
        player2counter = 'X'
        
        while(True):
            print_new_game_menu()
            choice = raw_input('Enter an option...')
            if choice == '1':
                size = get_board_size()
                b = GameBoard('H', 'R', size, size)
                result, winner = game_loop(self.game_loop_choice, print_board, b)
                self.handle_results(result, winner)
                break
            elif choice == '2':
                size = get_board_size()
                b = GameBoard('H', 'C', size, size)
                result, winner = game_loop(self.game_loop_choice, print_board, b)
                self.handle_results(result, winner)
                break
            elif choice == '3':
                size = get_board_size()
                b = GameBoard('H', 'H', size, size)
                result, winner = game_loop(self.game_loop_choice, print_board, b)
                self.handle_results(result, winner)
                break
            else:
                print_error('%s is not a valid option' %choice)

    def save_game_loop(self, board):
        """A loop to display the save game menu."""
        while(True):
            print_save_menu()
            choice = raw_input('Enter a name for the save file...')
            if not self.saveManager.save_game(choice, board):
                while(True):
                    print_error('The save name %s is already in use.' %choice)
                    overwrite = raw_input('Overwrite it? (y/n)...')
                    if overwrite == 'y' or overwrite == 'Y':
                        self.saveManager.delete_save_game(choice)
                    elif overwrite == 'n' or overwrite == 'N':
                        break
                    else:
                        print_error('%s is not a valid option' %overwrite)
            else:
                break
        print('New save "%s" created.' %choice)
                

    def load_game_loop(self):
        """A loop to display the load game menu."""
        while(True):
            print_load_menu()
            choice = raw_input('Enter an option...')
            # List all the avaiable save games
            if choice == '1':
                print_saves_list(self.saveManager.return_save_names())
            # Enter the name of the save file to load
            elif choice == '2':
                c = raw_input('Enter the name of the save you wish to load...')
                # If the save file does not exist
                if c not in self.saveManager.return_save_names():
                    print_error('%s is not a valid save file name' %c)
                saveFile = self.saveManager.load_game(c)
                if saveFile == False:
                    print_error('Could not load the save file.')
                else:
                    b = saveFile
                    result, winner = game_loop(self.game_loop_choice, print_board, b)
                    self.handle_results(result, winner)
                    self.main_menu_loop()
                    break
            # Delete a save file
            elif choice == '3':
                while(True):
                    c = raw_input('Enter the name of the save you wish to delete...')
                    if c not in self.saveManager.return_save_names():
                        print_error('%s is not a valid save file name' %c)
                        break
                    else:
                        self.saveManager.delete_save_game(c)
                        print('The save file %s has been deleted.' %c)
                        break
            # Return to the previous menu
            elif choice == '4':
                break
            elif choice == '':
                continue
            else:
                print_error('%s is not a valid option' %choice)
        

    def in_game_menu_loop(self, board):
        """A loop to display the in game menu."""
        while(True):
            print_game_menu()
            choice = raw_input('Enter an option...')
            if choice == '1':
                break
            elif choice == '2':
                self.save_game_loop(board)
                break
            elif choice == '3':
                self.load_game_loop()
                break
            elif choice == '4':
                self.main_menu_loop()
                break
            elif choice == '5':
                print_game_paused()
                self.enter_to_continue()
            elif choice == '6':
                exit()
            elif choice == '':
                continue
            else:
                print_error('%s is not a valid option' %choice)

    def main_menu_loop(self):
        """A loop to display the main menu."""
        print_welcome_message()
        self.enter_to_continue()
        while(True):
            print_main_menu()
            choice = raw_input('Enter an option...')
            if choice == '1':
                self.new_game_menu_loop()
            elif choice == '2':
                self.load_game_loop()
            elif choice == '3':
                print_credits()
                self.enter_to_continue()
            elif choice == '4':
                exit()
            elif choice == '':
                continue
            else:
                print_error('%s is not a valid option' %choice)
