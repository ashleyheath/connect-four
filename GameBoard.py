class GameBoard:
    """A class to store all the information needed to keep track of a game including information needed to save the state of one."""
    def __init__(self, player1type, player2type, boardWidth = 10, boardHeight = 10):
        self.pattern = 4
        self.actualBoardWidth = boardWidth
        self.actualBoardHeight = boardHeight
        self.forBoardWidth = self.actualBoardWidth+1
        self.forBoardHeight = self.actualBoardHeight+1
        self.boardState = {}
        self.player2type = player2type
        self.player1type = player1type
        self.player1counter = 'O'
        self.player2counter = 'X'
        self.player1Turn = True # Is it player 1's turn?

    def return_board_width(self):
        """Returns the actual width of the board."""
        return self.actualBoardWidth

    def return_board_height(self):
        """Returns the actual height of the board."""
        return self.actualBoardHeight

    def return_board_state(self):
        """Return a dictionary containing the state of the entire board."""
        return self.boardState

    def return_board_pattern(self):
        """Returns the length of the pattern of counters necessary to win."""
        return self.pattern

    def return_board_state_xy(self, key):
        """Return the state of the board at the coordinates X and Y. If (X,Y) does not exist return False"""
        if key in self.boardState:
            return self.boardState[key]
        else:
            return None

    def return_player_1_counter(self):
       return self.player1counter

    def return_player_2_counter(self):
       return self.player2counter

    def return_player_1_type(self):
        return self.player1type

    def return_player_2_type(self):
        return self.player2type

    def return_player_1_turn(self):
        return self.player1Turn

    def add_counter(self, xPosition, counterType):
        """Enter a new counter into the board."""
        # Make sure the column exists
        if xPosition < 1 or xPosition > self.actualBoardWidth:
            return False
        # If the column is full return False.
        if (xPosition, 1) in self.boardState:
            return False            
        # Working from the top (1) downwards (boardHeight),
        for i in range(2,self.forBoardHeight):
            # If the current point has a counter in it,
            if (xPosition, i) in self.boardState:
                # Add the counter to the previous point.
                key = (xPosition, i-1)
                self.boardState[key] = counterType
                self.player1Turn = not self.player1Turn
                return True
        # Else add it to the bottom
        key = (xPosition, self.actualBoardHeight)
        self.boardState[key] = counterType
        self.player1Turn = not self.player1Turn # Other player's turn
        return True

    def win_check(self):
        """Check if the conditions for victory have been met."""
        
        def horizontal_check(pattern = self.pattern):
            """Check to see if there is a horizontal pattern match."""
            tokens = []
            rows = []
            for y in range(1, self.forBoardHeight):
                s = ''
                for x in range(1, self.forBoardWidth):
                    t = str(self.return_board_state_xy((x,y)))
                    s = s + t
                    if t not in tokens and t != 'None':
                        tokens.append(t)
                rows.append(s)
            for i in rows:
                for j in tokens:
                    if j*pattern in i:
                        return True, j
            return False, None
        
        def vertical_check(pattern = self.pattern):
            """Check to see if there is a vertical pattern match."""
            tokens = []
            columns = []
            for x in range(1, self.forBoardWidth):
                s = ''
                for y in range(1, self.forBoardHeight):
                    t = str(self.return_board_state_xy((x,y)))
                    s = s + t
                    if t not in tokens and t != 'None':
                        tokens.append(t)
                columns.append(s)
            for i in columns:
                for j in tokens:
                    if j*pattern in i:
                        return True, j
            return False, None

        def diagonal_right_and_down_check(pattern = self.pattern):
            """Check to see if there is a diagonal pattern match heading right and downwards."""
            if self.actualBoardWidth < pattern or self.actualBoardHeight < pattern:
                return False, None
            tokens = []
            diagonals = []
            # For each square
            for x in range(1, self.forBoardWidth):
                for y in range(1, self.forBoardHeight):
                    currentToken = self.return_board_state_xy((x,y))
                    if currentToken == None:
                        continue
                    tokenCount = 1
                    for i in range(1, self.pattern):
                        token = self.return_board_state_xy((x+i,y+i))
                        if token == currentToken:
                            tokenCount += 1
                        else:
                            break
                        if tokenCount == self.pattern:
                            return True, currentToken
            return False, None
        
        def diagonal_right_and_up_check(pattern = self.pattern):
            """Check to see if there is a diagonal pattern match heading right and upwards."""
            if self.actualBoardWidth < pattern or self.actualBoardHeight < pattern:
                return False, None
            tokens = []
            diagonals = []
            # For each square
            for x in range(1, self.forBoardWidth):
                for y in range(1, self.forBoardHeight):
                    currentToken = self.return_board_state_xy((x,y))
                    if currentToken == None:
                        continue
                    tokenCount = 1
                    for i in range(1, self.pattern):
                        token = self.return_board_state_xy((x+i,y-i))
                        if token == currentToken:
                            tokenCount += 1
                        else:
                            break
                        if tokenCount == self.pattern:
                            return True, currentToken
            return False, None
            
        hct, hcm = horizontal_check()
        vct, vcm = vertical_check()
        drdct, drdcm = diagonal_right_and_down_check()
        druct, drucm = diagonal_right_and_up_check()

        if hct:
            return True, hcm
        elif vct:
            return True, vcm
        elif drdct:
            return True, drdcm
        elif druct:
            return True, drucm
        else:
            return False, None
        
    def full_check(self):
        """Check to see if the board is full."""
        full = True
        for i in range(1,self.forBoardWidth):
            for j in range(1,self.forBoardHeight):
                if (i,j) not in self.boardState:
                    full = False
                    break
            if full == False:
                break
        return full
