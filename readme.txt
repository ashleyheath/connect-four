1. Running The Game
	Navigate into the game folder('~/ConnectFour/') and use the command 'python connectFour.py' to run the game.
	By default, the game runs using a text-based interface in the command line. However, you can use the optional command line arguments '--text' and '--gui' to specify whether the game should run using either the text interface or the graphical interface respectively.

2. Gameplay
	Player 1 is always a human player. Player 2 is either a human or a computer controlled opponent depending on the options picked. There are two levels of AI opponent: Easy and Difficult.  NOTE: The size of your board is capped at 15 by 15 in order to keep it easy to see what is going on.

3. Command Line Interface
	This is the game's default interface. It may also be explicitly called by using the '--text' command line option.
	Follow the instructions on screen to navigate the menus. Enter the number linked to the action you want to perform. While in the game you can enter the number of the column to drop a counter or 'm' to go to the in game menu. When in command line mode you are limited to the save files contained in your save folder in the game directory. However, you are able to delete save files from in command line mode. Player 1's counter is a 'O' and player 2's counter is a 'X'.

4. Graphical User Interface
	This is a graphical interface created using Tkinter for Python. It is disabled by default but you can enable it by running the game using the '--gui' command line option. Using the gui allows you to open save files located outside of your save folder but does not allow you to delete them. Select an option from the drop down menus. During the game, click in the column where you wish to drop a counter. If the saving or loading of a file fails in gui mode there will be no error message, rather nothing will happen, indicating that the action was unsuccessful. The area at the bottom of the board displays information about the game in progress such as who wins and whether the game is paused or not. Player 1's counter is red and player 2's counter is blue.

5. Features:
	-Graphical and text-based interfaces
	-Pause, save, load and delete games
	-Choose the size of the board (anything greater than 4 by 4 but less than 16 by 16)

NOTE: Be careful not to click on the board when using the menus as this can make you waste your turn.
NOTE(2): If you load a game while there is a game in progress the game in progress will be lost.
