from os import system

def clear():
    """Clear the console."""
    system('clear')

def print_board(board):
    """Prints the connect 4 board to the console."""
    clear()
    player1Counter = board.return_player_1_counter()
    player2Counter = board.return_player_2_counter()
    actualBoardWidth = board.return_board_width()
    actualBoardHeight = board.return_board_height()
    forBoardWidth = actualBoardWidth + 1
    forBoardHeight = actualBoardHeight + 1

    boardState = board.return_board_state()
    
    # Setup the width of each square (where the Xs and Os are displayed)
    squareSize = int()# Size of the square is the length of the string of the width (or height)
    if actualBoardWidth > actualBoardHeight:
        squareSize = len(str(actualBoardWidth))
    else:
        squareSize = len(str(actualBoardHeight))
    if squareSize % 2 == 0:
        squareSize += 1
    # Create the text 'image' of the board
    boardString = ' '*(squareSize) + '|' # The string that holds the text 'image' of the board
    for i in range(1,forBoardWidth): # The numbers at the top of the column
        boardString += '_'*(squareSize-len(str(i))) + str(i) + '|'
    boardString += '\n'
    # The board itself
    for j in range(1,forBoardHeight):
        # The numbers at the side of the rows
        boardString += ' '*(squareSize-len(str(j))) + str(j)+ '|'
        for i in range(1,forBoardWidth):
            if (i,j) in boardState:
                halfsq = squareSize/2
                boardString += ' '*halfsq + str(boardState[(i,j)]) + ' '*halfsq + '|'
            else:
                boardString += ' '*squareSize + '|'
        boardString += '\n'
    print boardString

def print_error(errMsg):
    """Print a standard error message to the console but incorperate details provided by the argument."""
    print '\n'
    print '####ERROR####'
    print errMsg
    print '\n\n'
   
def print_game_paused():
    """Print the game paused 'menu'."""
    clear()
    print '-----------'
    print 'Game Paused'
    print '-----------'

def print_game_menu():
    """Print the in game menu."""
    clear()
    print '-----------'
    print '  Options  '
    print '-----------'
    print 'Option     Key'
    print 'Resume      1 '
    print 'Save        2 '
    print 'Load        3 '
    print 'Main Menu   4 '
    print 'Pause       5 '
    print 'Quit        6 '

welcomeMessage = '''\n
************************************************\n
  _____                            _     _  _   \n
 / ____|                          | |   | || |  \n
| |     ___  _ __  _ __   ___  ___| |_  | || |_ \n
| |    / _ \| '_ \| '_ \ / _ \/ __| __| |__   _|\n
| |___| (_) | | | | | | |  __/ (__| |_     | |  \n
 \_____\___/|_| |_|_| |_|\___|\___|\__|    |_|  \n
************************************************\n
*         Game created by Ashley Heath         *\n
************************************************\n'''
def print_welcome_message():
    """Print a welcome message when the program is initially run."""
    clear()
    print welcomeMessage

def print_main_menu():
    """Print the main menu."""
    clear()
    print '-----------'
    print '    Menu   '
    print '-----------'
    print 'Option     Key'
    print 'New Game    1 '
    print 'Load        2 '
    print 'Credits     3 '
    print 'Quit        4 '

credits = 'Game designed and developed by Ashley Heath.\nWelcome ascii art created using www.network-science.de/ascii/'
def print_credits():
    """Print the game credits."""
    clear()
    print credits

def print_new_game_menu():
    """Print the new game menu."""
    clear()
    print '-----------'
    print ' New  Game'
    print '-----------'
    print 'Option           Key'
    print 'vs Easy AI        1 '
    print 'vs Diificult AI   2 '
    print 'vs Human          3 '

def print_save_menu():
    """Print the save game menu."""
    print '------------'
    print '    Save    '
    print '------------'

def print_load_menu():
    """Print the load game menu."""
    print '------------'
    print '    Load    '
    print '------------'
    print 'Option     Key'
    print 'List Saves  1 '
    print 'Load Save   2 '
    print 'Delete Save 3 '
    print 'Previous Menu 4 '

def print_saves_list(savesList):
    """Print the names of the save files passed to the function in a list."""
    print 'The save files you currently have are:'
    for i in savesList:
        print i
    
