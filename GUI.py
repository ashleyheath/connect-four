from Tkinter import *
import tkFileDialog
import Text
import GameLoop
import GameBoard
import sys

X = None # x coordinate of the mouse when the board is clicked

def click(event):
    """Called when the board is clicked, updates the global X coordinate."""
    global X
    X = event.x

class GUI(Frame):
    """GUI for the Connect 4 board using Tkinter."""
    def __init__(self, saveManager, boardCanvasWidth=500, boardCanvasHeight=500+100):
        """Call to set the number of rows and columns NOTE: This must be called before you can begin using the GUI.
        Also holds all the hard-coded values for board width and height."""
        #_____ID_Storage_____
        self.pauseID = None
        self.textMessageIDs = []
        self.boardIDs = []

        self.gameInProgress = False

        self.saveManager = saveManager
        self.root = Tk()
        self.root.option_add('*tearOff', FALSE)
        self.root.title('Connect Four')

        self.boardCanvasWidth = boardCanvasWidth
        self.boardCanvasHeight = boardCanvasHeight
        Frame.__init__(self, self.root)
        #_____Menu_Setup_____
        self.menubar = Menu(self)
        # New game menu
        newGameMenu = Menu(self.menubar)
        self.menubar.add_cascade(label='New Game', menu=newGameMenu)
        newGameMenu.add_command(label='Human vs Human', command=self.h_vs_h_game_loop)
        newGameMenu.add_command(label='Human vs Easy AI', command=self.h_vs_r_game_loop)
        newGameMenu.add_command(label='Human vs Difficult AI', command=self.h_vs_c_game_loop)
        # In game menu
        gameMenu = Menu(self.menubar)
        self.menubar.add_cascade(label='Game Menu', menu=gameMenu)
        gameMenu.add_command(label='Load Game', command=self.draw_load_menu)
        gameMenu.add_command(label='Save Game', command=self.draw_save_menu)
        gameMenu.add_command(label='Pause/Unpause', command=self.draw_pause_game)
        # About menu
        aboutMenu = Menu(self.menubar)
        self.menubar.add_cascade(label='About', menu=aboutMenu)
        aboutMenu.add_command(label='Credits', command=self.draw_credits)
	# Exit menu
        exitMenu = Menu(self.menubar)
        self.menubar.add_cascade(label='Exit', menu=exitMenu)
        exitMenu.add_command(label='Quit Game', command=self.destroy)
        #
        self.root.config(menu=self.menubar)
        #_____End_Menu_Setup_____
        self.boardCanvas = Canvas(self.root, width=boardCanvasWidth, height=boardCanvasHeight)
        self.root.protocol('WM_DELETE_WINDOW', self.destroy)
        self.boardCanvas.bind('<Button-1>', click) # Bind the click function to the left mouse button
        self.board = None # Stores the last board drawn to the screen, used to allow saving the game currently in progress
        self.paused = False
        self.root.update()

    def delete_board(self):
        """Delete all the elements that make up the board."""
        try:
            for i in self.boardIDs:
                self.boardCanvas.delete(i)
                self.boardIDs.remove(i)
        # Catches an ID error that is caused if this is run after the window is destroyed
        except TclError:
            exit()

    def draw_board(self, boardObject):
        """Draw the board to the root window."""
        if self.paused == False and self.gameInProgress:
            player1counter = boardObject.return_player_1_counter()
            player2counter = boardObject.return_player_2_counter()
            self.delete_board()
            rows = boardObject.return_board_height()
            columns = boardObject.return_board_width()
            counterWidth = self.boardCanvasWidth / columns
            counterHeight = (self.boardCanvasHeight-100) / rows
            
            def draw_grid():
                """Draw the lines that make up the board."""
                for i in  range(0, self.boardCanvasWidth, counterWidth):
                    self.boardIDs.append(self.boardCanvas.create_line(i, 0, i, self.boardCanvasHeight-100))
                for i in range(0, self.boardCanvasHeight-100, counterHeight):
                    self.boardIDs.append(self.boardCanvas.create_line(0, i, self.boardCanvasWidth, i))

            def draw_counters():
                """Draw the counters currently in the board to the canvas.
                See: http://61.153.44.88/tkinter/tkinter-reference-a-gui-for-python/create_oval.html for a good description of how create_oval works."""
                player1colour = 'Red'
                player2colour = 'Blue'
                boardData = boardObject.return_board_state()
                for x in range(1, columns+1):
                    for y in range(1, rows+1):
                        if (x,y) in boardData:
                            playerCounter = boardData[(x,y)]
                            counterColour = None
                            if playerCounter == player1counter:
                                counterColour = player1colour
                            else:
                                counterColour = player2colour
                            topLeftCoordX = (x-1)*counterWidth
                            topLeftCoordY = (y-1)*counterHeight
                            bottomRightCoordX = x*counterWidth
                            bottomRightCoordY = y*counterHeight
                            self.boardIDs.append(self.boardCanvas.create_oval((topLeftCoordX, topLeftCoordY, bottomRightCoordX, bottomRightCoordY), fill=counterColour))
            draw_grid()
            draw_counters()
            self.boardCanvas.pack()
            self.board = boardObject
        self.root.update()
        
    def delete_text(self):
        """Delete the text currently in the text area of the board."""
        for i in self.textMessageIDs:
            self.boardCanvas.delete(i)
            self.textMessageIDs.remove(i)

    def draw_message(self, s):
        """Draw a message to the area below the board."""
        self.delete_text()
        tID = self.boardCanvas.create_text((0,self.boardCanvasHeight), text=s, anchor=SW)
        self.textMessageIDs.append(tID)
        
    def draw_error(self, s):
        """Display an error message."""
        self.delete_text()
        s = '#ERROR: ' + s
        tID = self.boardCanvas.create_text((0,self.boardCanvasHeight), text=s, anchor=SW)
        self.textMessageIDs.append(tID)

    def draw_pause_game(self):
        """Pause the game. Prevents new moves being made."""
        if self.paused == False:
            self.draw_message('Paused')
            self.paused = True
        else:
            self.delete_text()
            self.paused = False
        self.update()
    
    def draw_credits(self):
        """Draw a pop up with the credits information."""
        popUp = Toplevel()
        popUp.title("Credits")
        creditsPopUp = Message(popUp, text=Text.credits)
        creditsPopUp.pack()
        button = Button(popUp, text="Close", command=popUp.destroy)
        button.pack()

    def draw_save_menu(self):
        """Open a dialog to allow the user to save their current game."""
        if self.gameInProgress:
            while(True):
                saveName = tkFileDialog.asksaveasfilename(parent=self.root, title='Choose a name for the save file', filetypes = [('Save Games', '*.SAV')], initialdir = self.saveManager.return_save_directory()) # mode='rb',
                if self.saveManager.save_game_raw_name(saveName, self.board):
                    break

    def draw_load_menu(self):
        """Open a dialog to allow the user to load a save game."""
        while(True):
            try:
                saveName = tkFileDialog.askopenfilename(parent=self.root, title='Choose a save game to open', filetypes = [('Save Games', '*.SAV')], initialdir = self.saveManager.return_save_directory()) # mode='rb',
            except TypeError:
                break
            saveFile = self.saveManager.load_game_raw_name(saveName)
            if saveFile:
                self.gameInProgress = True
                self.delete_text()
                b = saveFile
                result, winner = GameLoop.game_loop(self.place_counter, self.draw_board, b)
                self.handle_results(result, winner)
                self.gameInProgress = False
                break
            else:
                break

    def handle_results(self, result, winner):
        """Display output approopriate to the results of a given game loop."""
        if result == 'WON':
            self.draw_message(winner+' wins!')
        elif result == 'FULL':
            self.draw_message('''It's a draw.''')
    
    def place_counter(self, boardObject):
        """Return which column to place the counter in based on the position of the mouse click."""
        if self.paused == False:
            self.columnChosen = None
            columns = boardObject.return_board_width()
            counterWidth = self.boardCanvasWidth / columns
            global X
            if X != None:
                column = -1
                for ix in range(0, self.boardCanvasWidth+counterWidth, counterWidth):
                    column += 1
                    if X > (ix-counterWidth) and X < ix:
                        self.columnChosen = column
                        X = None
                        return self.columnChosen

    def get_board_size(self):
        """Create a popup asking for the size of the board to create."""
        if self.board != None:
            # Prevents half drawn board on the screen
            self.draw_board(self.board)
        global num
        num = None
        v = StringVar()
        top = Toplevel()
        top.title('Enter a size for the board')
        entry = Entry(top, textvariable=v)
        entry.pack()
        v.set('10') # Set default size to 10
        entry.focus_set()
        def check():
            """Checks to see if the entered board size is allowed."""
            global num
            try:
                if int(v.get()) > 4 and int(v.get()) < 16:
                    num = int(v.get())
                    top.destroy()
            except ValueError:
                pass
        button = Button(top, text='Enter', command=check)
        button.pack()
        while(num==None):
            top.update()
            if num != None:
                break
        return num

    def h_vs_h_game_loop(self):
        """New game menu command for human vs human."""
        self.gameInProgress = True
        self.delete_text()
        self.delete_board()
        boardSize = self.get_board_size()
        b = GameBoard.GameBoard('H', 'H', boardSize, boardSize)
        result, winner = GameLoop.game_loop(self.place_counter, self.draw_board, b)
        self.handle_results(result, winner)
        self.gameInProgress = False
        
    def h_vs_r_game_loop(self):
        """New game menu command for human vs random ai."""
        self.gameInProgress = True
        self.delete_text()
        self.delete_board()
        boardSize = self.get_board_size()
        b = GameBoard.GameBoard('H', 'R', boardSize, boardSize)
        result, winner = GameLoop.game_loop(self.place_counter, self.draw_board, b)
        self.handle_results(result, winner)
        self.gameInProgress = False
        
    def h_vs_c_game_loop(self):
        """New game menu command for human vs checking ai."""
        self.gameInProgress = True
        self.delete_text()
        self.delete_board()
        boardSize = self.get_board_size()
        b = GameBoard.GameBoard('H', 'C', boardSize, boardSize)
        result, winner = GameLoop.game_loop(self.place_counter, self.draw_board, b)
        self.handle_results(result, winner)
        self.gameInProgress = False
        
    def start(self):
        """Start the mainloop of the root Tk object."""
        self.root.mainloop()
        
    def update(self):
        """Call the update function of the root Tk object."""
        self.root.update()

    def destroy(self):
        try:
            self.root.destroy()
        except:
            pass
        finally:
            sys.exit()

