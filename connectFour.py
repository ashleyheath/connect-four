#!/usr/bin/env python

# A connect four game

from sys import exit, argv
import SaveManager
import GUI
import TUI

def main(argv):
    """Main loop."""
    interfaceType = 'TEXT'
    if len(argv) > 1:
        option = argv[1]
        if option == '--gui':
            interfaceType = 'GUI'
        elif option == '--text':
            interfaceType = 'TEXT'
        else:
            print 'Command line options: Graphical interface "--gui", text based interface "--text".'
            exit()
    try:
        ThisSaveManager = SaveManager.SaveManager()
        if interfaceType == 'GUI':
            Gui = GUI.GUI(ThisSaveManager)
            Gui.start()
        elif interfaceType == 'TEXT':
            Tui = TUI.TUI(ThisSaveManager)
            Tui.main_menu_loop()
    except (KeyboardInterrupt, SystemExit):
        print '\n'
        exit()

if __name__ == '__main__':
        main(argv)
