import pickle
from os import getcwd, listdir, remove, mkdir
from GameBoard import GameBoard

class SaveManager:
    """Save manager class that keeps track of what save files are avaiable to the game."""    
    def load_save_names(self):
        """Load all the names of save files in the saves directory into the list saveNames.
           If the directory does not exist create one."""
        try:
            self.saveNames = []
            filesInDirectory = listdir(self.directory)
            for f in filesInDirectory:
                extension = f.rfind('.')
                if f[extension+1:] == 'SAV':
                    self.saveNames.append(f[:extension])
            self.saveNames.sort()
        except OSError:
            mkdir(self.directory)
            self.load_save_names()
        
    def __init__(self, directory = getcwd()+'/saves/'):
        """Find out if there are any games saved in the specified directory (if none is specified it defaults to the local directory)."""
        self.directory = directory
        if self.directory[-1] != '/':
            self.directory += '/'
        self.load_save_names()

    def return_save_directory(self):
        return self.directory

    def return_save_names(self):
        """Return a list of the save file names."""
        self.load_save_names()
        return self.saveNames
    
    def load_game(self, saveName):
        """Return a saved game state. Returns False if the hash in the file doesn't match the contents of the file."""
        try:
            saveFile = open(self.directory+saveName+'.SAV', 'r')
        except IOError:
            return False
        try:
            gameSave = pickle.load(saveFile)
        except AttributeError:
            return False
        saveFile.close()
        return gameSave

    def load_game_raw_name(self, rawSaveName):
        """Return a saved game state. Returns False if the hash in the file doesn't match the contents of the file."""
        rawSaveName = str(rawSaveName)
        try:
            saveFile = open(rawSaveName, 'r')
        except IOError:
            return False
        try:
            gameSave = pickle.load(saveFile)
        except AttributeError:
            return False
        saveFile.close()
        return gameSave

    def save_game(self, saveName, board):
        """Save a game state to file. Returns True for successful save. Returns False if the provided save name has already been used."""
        self.load_save_names()
        if saveName in self.saveNames:
            return False
        newSaveFile = open(self.directory+saveName+'.SAV', 'w')
        pickle.dump(board, newSaveFile)
        newSaveFile.flush()
        newSaveFile.close()
        return True

    def save_game_raw_name(self, rawSaveName, board):
        """Save a game state to file. Returns True for successful save."""
        rawSaveName = str(rawSaveName)
        newSaveFile = open(rawSaveName, 'w')
        pickle.dump(board, newSaveFile)
        newSaveFile.flush()
        newSaveFile.close()
        return True
    
    def delete_save_game(self, saveName):
        """Delete a save game based on the given save file name."""
        remove(self.directory+saveName+'.SAV')
        
