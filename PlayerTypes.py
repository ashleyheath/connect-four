from random import randint

class PlayerTypeAbstract:
    """The basic Player class."""
    def __init__(self, counterType):
        self.counterType = counterType

class Human(PlayerTypeAbstract):
    """Player type for human players."""
    def add_counter(self, board, xpos):
        """Add a counter to the given board at the given position."""
        return board.add_counter(xpos, self.counterType)

class RandomAI(PlayerTypeAbstract):
    """Player type for AI that places counters randomly."""
    def add_counter(self, board, spareVar=None): # spareVar maintains interchangeability with Human player type's addCounter
        """Add a counter to the given board."""
        while(True):
            xpos = randint(1, board.return_board_width())
            if board.add_counter(xpos, self.counterType):
                return True

class CheckingAI(PlayerTypeAbstract):
    """Player type for AI that analyses the board it is given and then chooses to place its counter at the end of the longest pattern of counters it can find.
    This means that it appears to be either extending its own chances of winning or cutting off your attempts to win."""
    def add_counter(self, board, spareVar=None): # spareVar maintains interchangeability with Human player type's addCounter
        """Add a counter to the given board."""
        # Checking Functions
        def check_horizontal(pattern):
            """Check for patterns of counters horizontally."""
            boardData = board.return_board_state()
            possibleX = []
            for y in range(1, board.return_board_width()+1):
                patternLength = 0
                currentCounter = None
                for x in range(1, board.return_board_height()+1):
                    if (x, y) in boardData:
                        if currentCounter != boardData[(x,y)]:
                            currentCounter = boardData[(x,y)]
                            patternLength = 1
                        else:
                            patternLength += 1
                        if patternLength == pattern:
                            if (x-patternLength+1,y) not in boardData:
                                possibleX.append(x-patternLength+1)
                            if (x+1,y) not in boardData:
                                possibleX.append(x+1)
            return possibleX
        
        def check_vertical(pattern):
            """Check for patterns of counters vertically."""
            boardData = board.return_board_state()
            possibleX = []
            for x in range(1, board.return_board_width()+1):
                patternLength = 0
                currentCounter = None
                for y in range(1, board.return_board_height()+1):
                    if (x,y) in boardData:
                        if currentCounter != boardData[(x,y)]:
                            currentCounter = boardData[(x,y)]
                            patternLength = 1
                        else:
                            patternLength += 1
                        if patternLength == pattern:
                            if y-patternLength > 0 and (x,y-patternLength) not in boardData:
                                possibleX.append(x)
            return possibleX
        
        # Add Counter Loop     
        while(True):
            boardPattern = board.return_board_pattern()
            # Look for patterns that are smaller than the final pattern
            for i in range(boardPattern):
                # Look for patterns in decreasing size
                searchPatternLength = boardPattern - i
                # Check horizontally
                x = check_horizontal(searchPatternLength)
                pos = False
                if len(x) > 0:
                    pos = x[randint(0,len(x)-1)]
                if x and pos and board.add_counter(pos, self.counterType):
                    return True
                # Check vertically
                x = check_vertical(searchPatternLength)
                pos = False
                if len(x) > 0:
                    pos = x[randint(0,len(x)-1)]
                if x and pos and board.add_counter(pos, self.counterType):
                    return True
            # If no matching patterns place a counter randomly
            x = randint(1, board.return_board_width())
            if board.add_counter(x, self.counterType):
                return True
