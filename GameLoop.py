from PlayerTypes import Human, RandomAI, CheckingAI

# Strings
player1Title = 'Player 1'
player2Title = 'Player 2'
    
def return_player_type(playerType, counterType):
    """Return the relevant PlayerType object which uses the specified counter."""
    if playerType == 'H':
        return Human(counterType)
    if playerType == 'R':
        return RandomAI(counterType)
    if playerType == 'C':
        return CheckingAI(counterType)

def return_player_title(board, counterType):
    """Returns the title of the player based on their counter type."""
    if counterType == board.return_player_1_counter():
        return player1Title
    else:
        return player2Title

def game_loop(choice_function, display_board_function, board, boardIsNew=True):
    """The game loop (where everything happens)."""
    player1Type = board.return_player_1_type()
    player2Type = board.return_player_2_type()
    player1 = return_player_type(player1Type, board.return_player_1_counter())
    player2 = return_player_type(player2Type, board.return_player_2_counter())
    
    while(True):
        p1 = board.return_player_1_turn()# Keeps track of who's turn it is
        # First, display the board on the screen        
        display_board_function(board)
        # Second, check if the game has been won or if the board is full
        won, counterType = board.win_check()
        if won:
            winner = return_player_title(board, counterType)
            del board
            return 'WON', winner
        elif board.full_check():
            del board
            return 'FULL', None
        # Third, if the game is still in progress and it is a human player's turn get their choice
        choice = None
        if (p1 and player1Type == 'H') or (not p1 and player2Type == 'H'):
            choice = choice_function(board)
            if choice == None:
                continue
        # Fourth, add a counter to the board based on the choice of the player (Human or AI)
        if p1:
            player1.add_counter(board, choice)
        else:
            player2.add_counter(board, choice)
